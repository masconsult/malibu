
package eu.masconsult.malibu.sqlite.test;

import java.io.IOException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.Suppress;
import eu.masconsult.malibu.sqlite.SQLiteMigrationOpenHelper;

public class SQLiteMigrationOpenHelperTest extends AndroidTestCase {

    private static final String DATABASE_NAME = "sample";

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        getContext().deleteDatabase(DATABASE_NAME);
    }

    @Override
    protected void tearDown() throws Exception {
        getContext().deleteDatabase(DATABASE_NAME);

        super.tearDown();
    }

    private MockHelper initHelper(int version, String databaseName) {
        return new MockHelper(getContext(), databaseName,
                SQLiteMigrationOpenHelper.DEFAULT_MIGRATION_FOLDER + "-"
                        + version);
    }

    private MockHelper initHelper(int version) {
        return initHelper(version, DATABASE_NAME);
    }

    private SQLiteDatabase openDb(int version, int checkMigrations) {
        MockHelper helper = initHelper(version);
        SQLiteDatabase db = helper.getWritableDatabase();
        assertEquals(version, db.getVersion());
        assertEquals(checkMigrations, helper.migrations);
        return db;
    }

    private SQLiteDatabase openDb(int version) {
        return openDb(version, version);
    }

    public void testCreatingDatabase() {
        openDb(1).close();
    }

    public void testUpgradingDatabase() {
        openDb(1).close();
        openDb(2, 1).close();
    }

    // on pre 3.0 sdk this doesn't produce exception
    @Suppress
    public void testDowngradingDatabase() {
        try {
            openDb(2, 2).close();
            openDb(1, 0).close();
            fail("Expected SQLiteException never thrown");
        } catch (SQLiteException e) {
            // we expect this
            assertEquals("Can't downgrade database from version 2 to 1", e.getMessage());
        }
    }

    public void testMultipleDatabasesSimultaniously() {
        getContext().deleteDatabase("sample1");
        getContext().deleteDatabase("sample2");
        MockHelper helper1 = initHelper(2, "sample1");
        MockHelper helper2 = initHelper(1, "sample2");

        SQLiteDatabase db1 = helper1.getWritableDatabase();
        SQLiteDatabase db2 = helper2.getWritableDatabase();

        assertEquals(2, db1.getVersion());
        assertEquals(1, db2.getVersion());
        assertEquals(2, helper1.migrations);
        assertEquals(1, helper2.migrations);

        db1.close();
        db2.close();
        helper1.close();
        helper2.close();

        getContext().deleteDatabase("sample1");
        getContext().deleteDatabase("sample2");
    }

    private static final class MockHelper extends SQLiteMigrationOpenHelper {

        public int migrations = 0;

        public MockHelper(Context context, String name, String migrationFolder) {
            super(context, name, migrationFolder);
        }

        @Override
        protected void executeMigration(SQLiteDatabase db,
                String migrationFileName) throws IOException {
            migrations++;
            super.executeMigration(db, migrationFileName);
        }
    }
}
