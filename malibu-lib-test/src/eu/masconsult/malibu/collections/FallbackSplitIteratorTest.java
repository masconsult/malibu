
package eu.masconsult.malibu.collections;

import static org.hamcrest.Matchers.contains;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

public class FallbackSplitIteratorTest {

    private static final List<String> data = ImmutableList.of("a", "b", "c", "d", "e", "f", "g",
            "h", "i", "j");

    private final List<String> functionParams = new LinkedList<String>();

    private String crashOnInput = null;

    private final Function<List<String>, Void> mockFunction = new Function<List<String>, Void>() {
        @Override
        public Void apply(List<String> data) {
            String input = Joiner.on("").join(data);
            final boolean willCrash = input.equals(crashOnInput);
            if (willCrash) {
                input = input + '*';
            }
            functionParams.add(input);
            if (willCrash) {
                throw new RuntimeException("I was ordered to crash!");
            }
            return null;
        }
    };

    @Test
    public void shouldPerformOnAllItems() {
        FallbackSplitIterator.perform(data, mockFunction, 100);
        Assert.assertThat(functionParams, contains("abcdefghij"));
    }

    @Test
    public void shouldPerformOnSplit() {
        FallbackSplitIterator.perform(data, mockFunction, 3);
        Assert.assertThat(functionParams, contains("abc", "def", "ghi", "j"));
    }

    @Test
    public void shouldFallbackOnError() {
        crashOnInput = "def";
        FallbackSplitIterator.perform(data, mockFunction, 3, 2);
        // we don't want to reprocess chunks that passed. Onlt the remaining and
        // crashed chunk are rerun on next chunk size
        Assert.assertThat(functionParams, contains("abc", "def*", "de", "fg", "hi", "j"));
    }

    @Test(expected = RuntimeException.class)
    public void shouldCrashWhenFallbacksExhausted() {
        crashOnInput = "def";
        FallbackSplitIterator.perform(data, mockFunction, 3);
    }

    @Test(expected = RuntimeException.class)
    public void shouldCrashWhenNoFallbacks() {
        FallbackSplitIterator.perform(data, mockFunction);
    }
}
