
package eu.masconsult.malibu.collections;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class MalibuCollectionsTest {

    static final List<String> empty_data = ImmutableList.of();
    static final List<String> data_of_3 = ImmutableList.of("a", "b", "c");

    @Test
    public void splitEmptyShouldReturnEmpty() {
        assertThat(MalibuCollections.split(empty_data, 10),
                is(Matchers.<List<String>> empty()));
    }

    @Test
    public void splitAllInOneGroup() {
        List<List<String>> split = MalibuCollections.split(data_of_3, 10);
        assertThat(split, hasSize(1));
        assertThat(split, hasItem(data_of_3));
    }

    @Test
    public void splitAllInSeperateGroup() {
        List<List<String>> groups = MalibuCollections.split(data_of_3, 1);
        @SuppressWarnings("unchecked")
        List<String>[] expectedGroups = new List[data_of_3.size()];
        for (int i = 0; i < data_of_3.size(); i++) {
            expectedGroups[i] = ImmutableList.of(data_of_3.get(i));
        }
        assertThat(groups, hasSize(expectedGroups.length));
        assertThat(groups, hasItems(expectedGroups));
    }

    @Test
    public void splitIrregularGroups() {
        List<List<String>> groups = MalibuCollections.split(data_of_3, 2);
        @SuppressWarnings("unchecked")
        List<String>[] expectedGroups = new List[] {
                ImmutableList.of(data_of_3.get(0), data_of_3.get(1)),
                ImmutableList.of(data_of_3.get(2))
        };
        assertThat(groups, hasSize(expectedGroups.length));
        assertThat(groups, hasItems(expectedGroups));
    }

    @Test(expected = NullPointerException.class)
    public void splitNullShouldThrow() {
        MalibuCollections.split(null, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void splitByZeroShouldThrow() {
        MalibuCollections.split(empty_data, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void splitByNegShouldThrow() {
        MalibuCollections.split(empty_data, -1);
    }
}
