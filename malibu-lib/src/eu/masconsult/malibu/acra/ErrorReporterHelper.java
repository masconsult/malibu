
package eu.masconsult.malibu.acra;

import java.util.Arrays;

import org.acra.ErrorReporter;

import android.util.Log;

public class ErrorReporterHelper {

    private static final String TAG = ErrorReporterHelper.class.getSimpleName();

    public static void reportHandledException(Exception cause) {
        reportHandledException(null, cause);
    }

    public static void reportHandledException(String message, Exception cause) {
        try {
            Log.w(TAG, message != null ? message : cause.getMessage(), cause);
            final ErrorReporter reporter = org.acra.ErrorReporter.getInstance();
            cause = new HandledException(message, cause);
            // we don't need to flood with duplicated trace
            cause.setStackTrace(Arrays.copyOf(cause.getStackTrace(), 2));
            reporter.handleException(cause);
        } catch (Throwable t) {
            System.err.println("failed to report exception through ACRA");
            t.printStackTrace();
        }
    }

}
