
package eu.masconsult.malibu.acra;

public class HandledException extends RuntimeException {

    private static final long serialVersionUID = 1737795850897153932L;

    public HandledException() {
    }

    public HandledException(String message) {
        super(message);
    }

    public HandledException(Throwable cause) {
        super(cause);
    }

    public HandledException(String message, Throwable cause) {
        super(message, cause);
    }

}
