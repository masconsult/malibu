
package eu.masconsult.malibu.analytics;

import android.content.Context;
import android.os.Bundle;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.listener.OnCreateListener;
import eu.masconsult.malibu.lifecycle.listener.OnSaveInstanceStateListener;
import eu.masconsult.malibu.lifecycle.listener.OnStartListener;
import eu.masconsult.malibu.lifecycle.listener.OnStopListener;

public class TrackedPresenter implements OnCreateListener, OnStopListener, OnStartListener,
        OnSaveInstanceStateListener {

    Context context;

    public TrackedPresenter(Context context) {
        this.context = context;
        if (context instanceof LifecycleContext) {
            ((LifecycleContext) context).registerLifecycleListener(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        EasyTracker.getTracker().trackActivityRetainNonConfigurationInstance();
    }

    @Override
    public void onStart() {
        // This call will ensure that the Activity in question is tracked
        // properly,
        // based on the setting of ga_auto_activity_tracking parameter. It will
        // also ensure that startNewSession is called appropriately.
        EasyTracker.getTracker().trackActivityStart(context.getClass().getCanonicalName());
    }

    @Override
    public void onStop() {
        // This call is needed to ensure time spent in an Activity and an
        // Application are measured accurately.
        EasyTracker.getTracker().trackActivityStop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Only one call to setContext is needed, but additional calls don't
        // hurt
        // anything, so we'll always make the call to ensure EasyTracker gets
        // setup properly.
        EasyTracker.getTracker().setContext(context);
    }

}
