
package eu.masconsult.malibu.analytics;

public interface TrackingParams {

    String getVersion(String versionName, int versionCode);
}
