
package eu.masconsult.malibu.analytics;

import android.content.Context;

import com.zubhium.ZubhiumSDK;

import eu.masconsult.malibu.analytics.internal.ParameterLoaderImpl;

public class ZubhiumManager {

    private static final String ZUBHIUM_SECRET_KEY = "zubhium_secret_key";
    private static boolean initialized = false;
    private static ZubhiumSDK zsdk;

    public static void initialize(final Context context) {
        if (initialized) {
            return;
        }

        final ParameterLoaderImpl loader = new ParameterLoaderImpl(context);
        final String secret_key = loader.getString(ZUBHIUM_SECRET_KEY);
        zsdk = ZubhiumSDK.getZubhiumSDKInstance(context, secret_key, loader.getVersion());
        if (zsdk != null) {
            zsdk.enableCrashReporting(false);
        }
    }

    public static ZubhiumSDK getZsdk() {
        return zsdk;
    }
}
