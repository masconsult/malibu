
package eu.masconsult.malibu.camlib;

import java.io.IOException;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import eu.masconsult.malibu.R;
import eu.masconsult.malibu.camlib.constants.MessageIds;
import eu.masconsult.malibu.camlib.handler.BaseCaptureHandler;
import eu.masconsult.malibu.camlib.handler.ImageProcessor;
import eu.masconsult.malibu.camlib.timer.InactivityTimer;
import eu.masconsult.malibu.camlib.view.ViewfinderView;
import eu.masconsult.malibu.lifecycle.activity.LifecycleActivity;

public abstract class BaseCaptureActivity<T> extends LifecycleActivity implements
        SurfaceHolder.Callback {

    public static final String TAG = BaseCaptureActivity.class.getSimpleName();

    protected CameraManager cameraManager;
    protected BaseCaptureHandler<T> handler;

    protected ViewfinderView<T> viewfinderView;

    protected InactivityTimer inactivityTimer;

    protected View resultView;
    protected TextView statusView;
    protected FrameLayout baseCaptureLayout;
    private boolean hasSurface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setActivityContentView();
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // CameraManager must be initialized here, not in onCreate().
        cameraManager = new CameraManager(getApplication());

        viewfinderView = (ViewfinderView<T>) findViewById(R.id.viewfinder_view);
        if (viewfinderView != null) {
            viewfinderView.setCameraManager(cameraManager);
        }

        handler = null;

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            // The activity was paused but not stopped, so the surface still
            // exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            initCamera(surfaceHolder);
        } else {
            // Install the callback and wait for surfaceCreated() to init the
            // camera.
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        inactivityTimer.onResume();

    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        try {
            cameraManager.openDriver(surfaceHolder);
            // Creating the handler starts the preview, which can also throw a
            // RuntimeException.
            if (handler == null) {
                handler = createCaptureHandler();
            }
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
            displayFrameworkBugMessageAndExit();
        } catch (RuntimeException e) {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            Log.w(TAG, "Unexpected error initializing camera", e);
            displayFrameworkBugMessageAndExit();
        }
    }

    private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getApplicationInfo().name);
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton(android.R.string.ok, new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            Log.e(TAG,
                    "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {

    }

    @Override
    protected void onPause() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        inactivityTimer.onPause();
        cameraManager.closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    public void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.sendEmptyMessageDelayed(MessageIds.restartPreview, delayMS);
        }
    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    protected BaseCaptureHandler<T> createCaptureHandler() {
        return new BaseCaptureHandler<T>(this, cameraManager);
    }

    protected void setActivityContentView() {
        setContentView(R.layout.capture);
    }

    /**
     * A valid result has been found, so give an indication of success and show
     * the results.
     * 
     * @param result from processed image
     */

    public abstract void handleDecode(T result);

    /**
     * Creates instance of your custom imageProcessor
     * 
     * @return Instance of your custom imageProcessor
     */
    public abstract ImageProcessor<T> createImageProcessor();
}
