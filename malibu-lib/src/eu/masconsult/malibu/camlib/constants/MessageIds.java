
package eu.masconsult.malibu.camlib.constants;

public interface MessageIds {

    public static final int autoFocus = 0;
    public static final int restartPreview = 1;
    public static final int decodeSucceeded = 2;
    public static final int decodeFailed = 3;
    public static final int returnScanResult = 4;
    public static final int decode = 5;
    public static final int quitDecode = 6;

}
