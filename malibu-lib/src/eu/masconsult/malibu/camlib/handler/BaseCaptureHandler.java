
package eu.masconsult.malibu.camlib.handler;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import eu.masconsult.malibu.camlib.BaseCaptureActivity;
import eu.masconsult.malibu.camlib.CameraManager;
import eu.masconsult.malibu.camlib.constants.MessageIds;

public class BaseCaptureHandler<T> extends Handler implements MessageIds {

    public static final String TAG = BaseCaptureHandler.class.getSimpleName();

    protected final BaseCaptureActivity<T> captureActivity;
    protected final CameraManager cameraManager;
    protected State state;

    private final BaseDecodeThread<T> decodeThread;

    protected enum State {
        PREVIEW, SUCCESS, DONE
    }

    public BaseCaptureHandler(BaseCaptureActivity<T> captureActivity, CameraManager cameraManager) {
        this.captureActivity = captureActivity;
        state = State.SUCCESS;
        // Start ourselves capturing previews and decoding.
        this.cameraManager = cameraManager;
        decodeThread = new BaseDecodeThread<T>(this.captureActivity);
        decodeThread.start();
        startPreviewAndDecode();
    }

    protected void startPreviewAndDecode() {
        cameraManager.startPreview();
        restartPreviewAndDecode();
    }

    protected void restartPreviewAndDecode() {
        if (state == State.SUCCESS) {
            state = State.PREVIEW;
            cameraManager.requestPreviewFrame(decodeThread.getHandler(), decode);
            cameraManager.requestAutoFocus(this, autoFocus);
            captureActivity.drawViewfinder();
        }
    }

    public void quitSynchronously() {
        state = State.DONE;
        cameraManager.stopPreview();
        Message quit = Message.obtain(decodeThread.getHandler(), quitDecode);
        quit.sendToTarget();
        try {
            // Wait at most half a second; should be enough time, and onPause()
            // will timeout quickly
            decodeThread.join(500L);
        } catch (InterruptedException e) {
            // continue
        }

        // Be absolutely sure we don't send any queued up messages
        removeMessages(decodeSucceeded);
        removeMessages(decodeFailed);
    }

    @Override
    public void handleMessage(Message message) {
        switch (message.what) {
            case autoFocus:
                onAutoFocusMessageRecieved(message);
                break;
            case restartPreview:
                Log.d(TAG, "Got restart preview message");
                onRestartPreviewMessageRecieved(message);
                break;
            case decodeSucceeded:
                Log.d(TAG, "Got decode succeeded message");
                state = State.SUCCESS;
                onDecodeSucceededMessageRecieved(message);
                break;
            case decodeFailed:
                // We're decoding as fast as possible, so when one decode fails,
                // start another.
                state = State.PREVIEW;
                onDecodeFailedMessageRecieved(message);
                break;
            case returnScanResult:
                onResultFromScanRecieved(message);
                break;

        }
    }

    protected void onAutoFocusMessageRecieved(Message message) {
        if (state == State.PREVIEW) {
            cameraManager.requestAutoFocus(this, autoFocus);
        }
    }

    protected void onRestartPreviewMessageRecieved(Message message) {
        restartPreviewAndDecode();
    }

    protected void onDecodeSucceededMessageRecieved(Message message) {
        captureActivity.handleDecode((T) message.obj);
    }

    protected void onDecodeFailedMessageRecieved(Message message) {
        cameraManager.requestPreviewFrame(decodeThread.getHandler(), decode);
    }

    protected void onResultFromScanRecieved(Message message) {
        Log.d(TAG, "Got return scan result message");
        captureActivity.setResult(Activity.RESULT_OK, (Intent) message.obj);
        captureActivity.finish();
    }
}
