/*
 * Copyright (C) 2010 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.masconsult.malibu.camlib.handler;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import eu.masconsult.malibu.camlib.BaseCaptureActivity;
import eu.masconsult.malibu.camlib.constants.MessageIds;

public class BaseDecodeHandler<T> extends Handler implements MessageIds {

    protected final BaseCaptureActivity<T> activity;

    private boolean running = true;

    private ImageProcessor<T> imageProcessor;

    protected BaseDecodeHandler(BaseCaptureActivity<T> activity, ImageProcessor<T> imageProcessor) {
        this.activity = activity;
        this.imageProcessor = imageProcessor;
    }

    @Override
    public void handleMessage(Message message) {
        if (!running) {
            return;
        }
        switch (message.what) {
            case decode:
                decode((byte[]) message.obj, message.arg1, message.arg2);
                break;
            case quitDecode:
                running = false;
                Looper.myLooper().quit();
                break;
        }
    }

    /**
     * Decode the data within the viewfinder rectangle, and time how long it
     * took. For efficiency, reuse the same reader objects from one decode to
     * the next.
     * 
     * @param data The YUV preview frame.
     * @param width The width of the preview frame.
     * @param height The height of the preview frame.
     */
    private void decode(byte[] data, int width, int height) {
        onCameraRawImageFrameRecieved(data, width, height);
    }

    protected void onCameraRawImageFrameRecieved(byte[] data, int width, int height) {
        Object result = imageProcessor.processRawImage(data, width, height);

        Handler handler = activity.getHandler();
        if (result != null) {
            if (handler != null) {
                sendDecodeSucceededMessageToTarget(handler, result);
            }
        } else {
            if (handler != null) {
                sendDecodeFailedMessageToTarget(handler);
            }
        }
    }

    protected void sendDecodeSucceededMessageToTarget(Handler handler,
            Object result) {
        Message message = Message
                .obtain(handler, decodeSucceeded, result);
        message.sendToTarget();
    }

    protected void sendDecodeFailedMessageToTarget(Handler handler) {
        Message message = Message.obtain(handler, decodeFailed);
        message.sendToTarget();
    }
}
