
package eu.masconsult.malibu.camlib.handler;

import java.util.concurrent.CountDownLatch;

import android.os.Handler;
import android.os.Looper;
import eu.masconsult.malibu.camlib.BaseCaptureActivity;

public class BaseDecodeThread<T> extends Thread {

    public static final String BITMAP = "bitmap";

    protected final BaseCaptureActivity<T> captureActivity;

    protected Handler handler;
    protected final CountDownLatch handlerInitLatch;

    private ImageProcessor<T> imageProcessor;

    public BaseDecodeThread(BaseCaptureActivity<T> captureActivity) {
        this.captureActivity = captureActivity;
        handlerInitLatch = new CountDownLatch(1);
        imageProcessor = captureActivity.createImageProcessor();
    }

    public Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }

    @Override
    public void run() {
        Looper.prepare();
        handler = new BaseDecodeHandler<T>(captureActivity, imageProcessor);
        handlerInitLatch.countDown();
        Looper.loop();
    }
}
