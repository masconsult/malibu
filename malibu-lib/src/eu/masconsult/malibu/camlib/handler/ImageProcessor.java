package eu.masconsult.malibu.camlib.handler;

public interface ImageProcessor<T> {

	/**
	 * Process raw image data.
	 * 
	 * @param data
	 * @param width
	 * @param height
	 * @return result object of type <T>
	 */
	T processRawImage(byte[] data, int width, int height);

}
