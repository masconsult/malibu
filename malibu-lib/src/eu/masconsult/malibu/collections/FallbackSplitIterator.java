
package eu.masconsult.malibu.collections;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Function;

import eu.masconsult.malibu.acra.ErrorReporterHelper;

public final class FallbackSplitIterator {

    /**
     * A routine to split a list of data into chunks, and execute given
     * operation on the chunks. If the operation crashes the list is divided
     * using the next chunk size and operation is reexecuted. This process is
     * repeated until operation completes successfully or the chunk sizes are
     * exhausted. If the chunk sizes are exhausted the last operation exception
     * is retrown.
     * 
     * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
     */
    public static <T> void perform(List<T> data, Function<List<T>, Void> function,
            int... maxSizes) {
        checkNotNull(function, "function should not be null");
        checkArgument(maxSizes.length > 0, "no split sizes defined");

        int succeededIndex = 0;
        RuntimeException error = null;
        for (int maxSize : maxSizes) {
            try {
                for (List<T> split : MalibuCollections.split(
                        data.subList(succeededIndex, data.size()), maxSize)) {
                    function.apply(split);
                    succeededIndex += split.size();
                }
                return;
            } catch (RuntimeException re) {
                error = re;
                ErrorReporterHelper.reportHandledException("Fallback cought an exception", re);
                continue;
            }
        }
        throw error;
    }

}
