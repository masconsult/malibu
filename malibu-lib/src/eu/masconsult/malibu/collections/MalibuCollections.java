
package eu.masconsult.malibu.collections;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

/**
 * Helpful routines to perform operations on collections not found in java6 and
 * guava
 * 
 * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
 */
public class MalibuCollections {

    /**
     * Split a collection into chunks with given size. All chunks except the
     * last one are guaranteed to contain exactly <code>maxSize</code> items.
     * 
     * @param src a {@link List} with all items
     * @param maxSize maximum size that a chunk can have
     * @return a {@link List} containing several chunks, each chunk is a
     *         {@link List} that contain no more than <code>maxSize</code>
     *         items.
     */
    public static <T> List<List<T>> split(List<T> src, int maxSize) {
        checkNotNull(src, "source collection cannot be null");
        checkArgument(maxSize > 0, "splitting can only be done in positive sized groups");

        // we use the builder to generate the list of chunks
        final Builder<List<T>> builder = ImmutableList.builder();

        // if there are no items, just return empty list
        if (src.size() == 0) {
            return ImmutableList.of();
        }

        // if we have only one group, no need to create new collection
        if (src.size() <= maxSize) {
            builder.add(src);
        } else {
            int endIdx;
            for (int startIdx = 0, len = src.size(); startIdx < len;) {
                endIdx = startIdx + maxSize;
                builder.add(src.subList(startIdx, Math.min(endIdx, len)));
                startIdx = endIdx;
            }
        }

        // return the list of chunks
        return builder.build();
    }
}
