
package eu.masconsult.malibu.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;
import eu.masconsult.malibu.acra.ErrorReporterHelper;

/**
 * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
 */
public class ExportDatabaseFileTask extends AsyncTask<String, Void, Boolean> {

    private Context ctx;
    private String packageName;
    private Exception error;
    private File copiedFile;

    public ExportDatabaseFileTask(Context ctx) {
        this.ctx = ctx.getApplicationContext();
        packageName = ctx.getApplicationContext().getPackageName();
    }

    // automatically done on worker thread (separate from UI thread)
    @Override
    protected Boolean doInBackground(final String... args) {
        try {
            File dbFile = new File(Environment.getDataDirectory() + "/data/" + packageName
                    + "/databases/"
                    + args[1]);

            File exportDir = new File(Environment.getExternalStorageDirectory(), "");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            File file = new File(exportDir, dbFile.getName());

            try {
                file.createNewFile();
                copyFile(dbFile, file);
                copiedFile = file;
                return true;
            } catch (IOException e) {
                ErrorReporterHelper.reportHandledException(e);
                return false;
            }
        } catch (Exception e) {
            error = e;
            return false;
        }
    }

    // can use UI thread here
    @Override
    protected void onPostExecute(final Boolean success) {
        if (error != null) {
            ErrorReporterHelper.reportHandledException(error);
        }
        if (success) {
            onSuccess();
        } else {
            onError(error);
        }
    }

    protected void onSuccess() {
        Toast.makeText(ctx, "Export successful!", Toast.LENGTH_SHORT).show();
    }

    protected void onError(Exception error2) {
        Toast.makeText(ctx, "Export failed", Toast.LENGTH_SHORT).show();
    }

    void copyFile(File src, File dst) throws IOException {
        FileInputStream srcStream = new FileInputStream(src);
        try {
            FileOutputStream dstStream = new FileOutputStream(dst);
            try {
                FileChannel inChannel = srcStream.getChannel();
                FileChannel outChannel = dstStream.getChannel();
                try {
                    inChannel.transferTo(0, inChannel.size(), outChannel);
                } finally {
                    if (inChannel != null) {
                        inChannel.close();
                    }
                    if (outChannel != null) {
                        outChannel.close();
                    }
                }
            } finally {
                dstStream.close();
            }
        } finally {
            srcStream.close();
        }
    }

    protected Context getContext() {
        return ctx;
    }

    protected File getCopiedFile() {
        return copiedFile;
    }
}
