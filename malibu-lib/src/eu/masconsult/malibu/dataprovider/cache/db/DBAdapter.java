package eu.masconsult.malibu.dataprovider.cache.db;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import eu.masconsult.malibu.dataprovider.model.Model;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class DBAdapter<M extends Model> {

	Context context;
	DBHelper<M> dbHelper;
	ModelHelper<M> helper;
	private SQLiteDatabase db;

	public DBAdapter(Context context, ModelHelper<M> helper) {
		this.context = context;
		this.helper = helper;
	}

	private void open() {
		dbHelper = new DBHelper<M>(context, helper);
		db = dbHelper.getWritableDatabase();
		helper.setDb(db);
	}

	private void close() {
		dbHelper.close();
	}

	private void clearCache() {
		db.delete(helper.getTableName(), helper.getClearCacheFilter(),
				helper.getClearCacheParams());
	}

	public void cache(List<M> data) {
		open();
		clearCache();
		helper.cache(data);
		close();
	}

	public List<M> fetch() {
		open();
		final List<M> res = helper.fetch();
		close();
		return res;
	}

}
