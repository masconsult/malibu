package eu.masconsult.malibu.dataprovider.cache.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import eu.masconsult.malibu.dataprovider.model.Model;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class DBHelper<M extends Model> extends SQLiteOpenHelper {

	private static final String TAG = DBHelper.class.getSimpleName();

	public static final String DB_NAME = "cache.db";
	private static final int DB_VERSION = 1;

	private ModelHelper<M> helper;

	public DBHelper(Context context, ModelHelper<M> helper) {
		super(context, DB_NAME + helper.getTableName(), null, DB_VERSION);
		this.helper = helper;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(helper.getCreateTableString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(TAG, "upgrading db from " + oldVersion + " to " + newVersion);
		onCreate(db);
	}
}
