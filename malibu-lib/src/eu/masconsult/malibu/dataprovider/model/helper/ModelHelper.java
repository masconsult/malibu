package eu.masconsult.malibu.dataprovider.model.helper;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.json.JSONException;

import android.database.sqlite.SQLiteDatabase;
import eu.masconsult.malibu.dataprovider.model.Model;

public abstract class ModelHelper<M extends Model> {

	protected SQLiteDatabase db;

	public void setDb(SQLiteDatabase db) {
		this.db = db;
	}

	public abstract String getTableName();

	public abstract String getCreateTableString();

	public abstract void cache(List<M> data);

	public abstract List<M> fetch();

	public abstract List<M> retrieveDataFromServer(HttpClient client)
			throws ClientProtocolException, IOException, JSONException;

	public String getClearCacheFilter() {
		return null;
	}

	public String[] getClearCacheParams() {
		return null;
	}
}
