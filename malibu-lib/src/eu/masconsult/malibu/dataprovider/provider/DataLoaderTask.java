package eu.masconsult.malibu.dataprovider.provider;

import java.util.List;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import eu.masconsult.malibu.dataprovider.model.Model;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class DataLoaderTask<M extends Model, H extends ModelHelper<M>> extends
		AsyncTask<Void, Void, List<M>> {

	private HttpClient client;

	private DataProvider<M, H> provider;

	public DataLoaderTask(DataProvider<M, H> provider) {
		this.provider = provider;
		client = new DefaultHttpClient();
		client.getParams().setParameter("http.protocol.version",
				HttpVersion.HTTP_1_1);
	}

	@Override
	protected List<M> doInBackground(Void... params) {
		try {
			return provider.getHelper().retrieveDataFromServer(client);
		} catch (Exception e) {
			provider.notifyError();
		}
		return null;
	}

	@Override
	protected void onPostExecute(List<M> result) {
		super.onPostExecute(result);
		provider.resultReceived(result);
	}

}
