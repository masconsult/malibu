
package eu.masconsult.malibu.dataprovider.provider;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import eu.masconsult.malibu.dataprovider.cache.db.DBAdapter;
import eu.masconsult.malibu.dataprovider.model.Model;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.listener.OnPauseListener;
import eu.masconsult.malibu.lifecycle.listener.OnResumeListener;

public class DataProvider<M extends Model, H extends ModelHelper<M>> implements
        OnResumeListener, OnPauseListener {

    DataProviderListener<M> listener;
    DataLoaderTask<M, H> loaderTask;
    Context context;
    DBAdapter<M> dbAdapter;
    H helper;

    DataProviderPolicy policy = DataProviderPolicy.ALWAYS_SERVER;

    ConnectionObserver broadcastConnectionReciever;

    /**
     * Indicates if data need to be refreshed
     */
    boolean needRefresh;

    /**
     * indicates if get data service is running
     */
    boolean isGettingServerData;
    private boolean forceRefresh;

    public DataProvider(Context context, DataProviderListener<M> listener,
            H helper) {
        this.context = context;
        this.listener = listener;
        this.helper = helper;
        init();
    }

    /**
     * Initialize data provider. This method is called when provider is created
     */
    private void init() {
        needRefresh = true;
        forceRefresh = false;
        dbAdapter = new DBAdapter<M>(context, helper);
        if (context instanceof LifecycleContext) {
            ((LifecycleContext) context).registerLifecycleListener(this);
        }
    }

    /**
     * Provider starts retrieving data
     */
    @Override
    public void onResume() {
        registerConnectionObserver();
        boolean hasCachedData = getCachedData();

        if (policy.equals(DataProviderPolicy.FIRST_CACHE_THEN_SERVER) && hasCachedData
                && !forceRefresh) {
            needRefresh = false;
        }

        getServerData();
    }

    /**
     * provider stops retrieving data
     */
    @Override
    public void onPause() {
        unregisterConnectionObserver();
        if (loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }
        isGettingServerData = false;
        notifyStop();
    }

    /**
     * Refresh data
     */
    public void refresh() {
        needRefresh = true;
        forceRefresh = true;
        onResume();
    }

    public H getHelper() {
        return helper;
    }

    protected void notifyStart() {
        listener.onProviderStart();
    }

    protected void notifyStop() {
        listener.onProviderStop();
    }

    protected void notifyError() {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listener.onProviderError();
            }
        });
    }

    /**
     * Send received data to listener
     * 
     * @param data
     */
    protected void fillData(long timestamp, List<M> data) {
        listener.onDataChanged(timestamp, data);
    }

    /**
     * Called when service get the data
     * 
     * @param data
     */
    protected void resultReceived(List<M> data) {
        onPause();
        if (data != null && !data.isEmpty()) {
            needRefresh = false;
            cacheData(data);
            fillData(System.currentTimeMillis(), data);
        }
    }

    /**
     * Retrieves data from server
     */
    protected void getServerData() {
        if (hasConnection() && needRefresh && !isGettingServerData) {
            isGettingServerData = true;
            loaderTask = new DataLoaderTask<M, H>(this);
            loaderTask.execute();
            notifyStart();
        }
    }

    /**
     * Get cached data
     */
    protected boolean getCachedData() {
        // TODO get data asynchronous and retrieve the timestamp of the data
        List<M> data = dbAdapter.fetch();
        fillData(0, data);
        return data != null && data.size() > 0;
    }

    /**
     * Store received data
     * 
     * @param data
     */
    protected void cacheData(List<M> data) {
        dbAdapter.cache(data);
    }

    protected void onConnect() {
        getServerData();
    }

    protected void onDisconnect() {

    }

    private boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    class ConnectionObserver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(
                    ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (hasConnection()) {
                    onConnect();
                } else {
                    onDisconnect();
                }
            }

        }
    }

    private void registerConnectionObserver() {
        if (broadcastConnectionReciever == null) {
            broadcastConnectionReciever = new ConnectionObserver();
            context.registerReceiver(broadcastConnectionReciever,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

    }

    private void unregisterConnectionObserver() {
        if (broadcastConnectionReciever != null) {
            context.unregisterReceiver(broadcastConnectionReciever);
            broadcastConnectionReciever = null;
        }
    }

    public DataProvider<M, H> withPolicy(DataProviderPolicy policy) {
        this.policy = policy;
        return this;
    }

}
