package eu.masconsult.malibu.dataprovider.provider;

import java.util.List;

import eu.masconsult.malibu.dataprovider.model.Model;

public interface DataProviderListener<M extends Model> {

	void onProviderStart();

	void onProviderStop();

	void onProviderError();

	void onDataChanged(long timestamp, List<M> data);

}
