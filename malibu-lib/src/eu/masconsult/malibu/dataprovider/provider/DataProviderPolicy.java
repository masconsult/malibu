
package eu.masconsult.malibu.dataprovider.provider;

public enum DataProviderPolicy {

    /**
     * Provider will always return cached data if is available, otherwise
     * retrieve data from server
     */
    FIRST_CACHE_THEN_SERVER,
    /**
     * Provider will always retrieve data from server
     */
    ALWAYS_SERVER;

}
