
package eu.masconsult.malibu.lifecycle;

import android.app.Activity;
import eu.masconsult.malibu.lifecycle.listener.OnDestroyListener;

/**
 * Helper to lifecycle enabled object that requires reference to the context.
 * 
 * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
 */
public class ActivityLifecycleObject implements OnDestroyListener {

    private Activity context;

    public ActivityLifecycleObject(LifecycleContext context) {
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException(
                    "Activity implementing LifecycleContext is required!");
        }
        context.registerLifecycleListener(this);
        this.context = (Activity) context;
    }

    public Activity getContext() {
        return context;
    }

    @Override
    public void onDestroy() {
        // release the reference to the context so we don't keep it from GC
        context = null;
    }
}
