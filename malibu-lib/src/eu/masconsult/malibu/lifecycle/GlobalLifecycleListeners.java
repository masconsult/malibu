
package eu.masconsult.malibu.lifecycle;

import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.Lists;

import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;

public class GlobalLifecycleListeners {

    private static final LinkedList<LifecycleInterface> objects = new LinkedList<LifecycleInterface>();
    private static final List<LifecycleInterface> robjects = Lists.reverse(objects);

    /**
     * register your common lifecycle handlers
     */
    public static void registerCommonLifecycleListener(LifecycleInterface lifecycleObject) {
        objects.add(lifecycleObject);
    }

    public static Iterable<LifecycleInterface> getListener() {
        return objects;
    }

    public static Iterable<LifecycleInterface> getReverseListener() {
        return robjects;
    }

}
