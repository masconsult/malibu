
package eu.masconsult.malibu.lifecycle;

import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;

public interface LifecycleContext {

    /**
     * The supplied object should implement some of
     * {@link eu.masconsult.malibu.lifecycle.listener} listeners
     * 
     * @param listener that implements some of the {@link LifecycleInterface}
     *            interfaces
     */
    void registerLifecycleListener(LifecycleInterface listener);

}
