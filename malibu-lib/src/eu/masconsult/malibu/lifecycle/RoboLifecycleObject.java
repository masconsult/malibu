
package eu.masconsult.malibu.lifecycle;

import roboguice.application.RoboApplication;
import android.content.Context;
import android.os.Bundle;
import eu.masconsult.malibu.lifecycle.listener.OnCreateListener;

/**
 * Helper for lifecycle enabled object that requires injection. Note: the
 * injection happens during onCreate
 * 
 * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
 */
public class RoboLifecycleObject extends ActivityLifecycleObject implements OnCreateListener {

    public RoboLifecycleObject(LifecycleContext context) {
        super(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        final Context appContext = getContext().getApplicationContext();
        if (!(appContext instanceof RoboApplication)) {
            throw new IllegalArgumentException(
                    "Supplied context is not part of RoboApplication");
        }
        // ask for injection
        ((RoboApplication) appContext).getInjector().injectMembers(this);
    }
}
