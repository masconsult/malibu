
package eu.masconsult.malibu.lifecycle.activity;

import roboguice.activity.RoboTabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.internal.LifecycleObjectHandler;
import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;

/**
 * @deprecated New applications should use Fragments instead of this class; to
 *             continue to run on older devices, you can use the v4 support
 *             library which provides a version of the Fragment API that is
 *             compatible down to android.os.Build.VERSION_CODES.DONUT.
 */
@Deprecated
public class LifecycleRoboTabActivity extends RoboTabActivity implements LifecycleContext {

    private LifecycleObjectHandler stateHolder = new LifecycleObjectHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateHolder.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        stateHolder.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        stateHolder.onResume();
    }

    @Override
    protected void onPause() {
        stateHolder.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stateHolder.onStop();
    }

    @Override
    protected void onDestroy() {
        stateHolder.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stateHolder.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        stateHolder.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        stateHolder.onBeforeSetContentView(this);
        super.setContentView(layoutResID);
        stateHolder.onAfterSetContentView(this);
    }

    @Override
    public void setContentView(View view) {
        stateHolder.onBeforeSetContentView(this);
        super.setContentView(view);
        stateHolder.onAfterSetContentView(this);
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        stateHolder.onBeforeSetContentView(this);
        super.setContentView(view, params);
        stateHolder.onAfterSetContentView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        stateHolder.onCreateOptionsMenu(menu, getMenuInflater());
        return stateHolder.hasOptionsMenu() || super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean result = stateHolder.hasOptionsMenu() || super.onPrepareOptionsMenu(menu);
        stateHolder.onPrepareOptionsMenu(menu);
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return stateHolder.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        stateHolder.onOptionsMenuClosed(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        stateHolder.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void registerLifecycleListener(LifecycleInterface object) {
        stateHolder.registerLifecycleListener(object);
    }
}
