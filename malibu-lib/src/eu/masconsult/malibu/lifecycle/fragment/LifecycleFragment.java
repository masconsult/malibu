
package eu.masconsult.malibu.lifecycle.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.internal.LifecycleObjectHandler;
import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;
import eu.masconsult.malibu.lifecycle.listener.OnRestoreInstanceStateListener;

public class LifecycleFragment extends Fragment implements LifecycleContext,
        OnRestoreInstanceStateListener {

    private LifecycleObjectHandler stateHolder = new LifecycleObjectHandler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateHolder.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        stateHolder.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        stateHolder.onResume();
        setHasOptionsMenu(stateHolder.hasOptionsMenu());
    }

    @Override
    public void onPause() {
        stateHolder.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        stateHolder.onStop();
    }

    @Override
    public void onDestroy() {
        stateHolder.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stateHolder.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        stateHolder.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        stateHolder.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        stateHolder.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return stateHolder.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        stateHolder.onOptionsMenuClosed(menu);
    }

    @Override
    public void registerLifecycleListener(LifecycleInterface object) {
        stateHolder.registerLifecycleListener(object);
    }
}
