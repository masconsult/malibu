
package eu.masconsult.malibu.lifecycle.internal;

import java.util.HashMap;
import java.util.LinkedList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import eu.masconsult.malibu.lifecycle.GlobalLifecycleListeners;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.listener.HasOptionsMenuListener;
import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;
import eu.masconsult.malibu.lifecycle.listener.OnActivityResultListener;
import eu.masconsult.malibu.lifecycle.listener.OnAfterSetContentViewListener;
import eu.masconsult.malibu.lifecycle.listener.OnBeforeSetContentViewListener;
import eu.masconsult.malibu.lifecycle.listener.OnCreateListener;
import eu.masconsult.malibu.lifecycle.listener.OnCreateOptionsMenuListener;
import eu.masconsult.malibu.lifecycle.listener.OnDestroyListener;
import eu.masconsult.malibu.lifecycle.listener.OnOptionsItemSelectedListener;
import eu.masconsult.malibu.lifecycle.listener.OnOptionsMenuClosedListener;
import eu.masconsult.malibu.lifecycle.listener.OnPauseListener;
import eu.masconsult.malibu.lifecycle.listener.OnPrepareOptionsMenuListener;
import eu.masconsult.malibu.lifecycle.listener.OnRestoreInstanceStateListener;
import eu.masconsult.malibu.lifecycle.listener.OnResumeListener;
import eu.masconsult.malibu.lifecycle.listener.OnSaveInstanceStateListener;
import eu.masconsult.malibu.lifecycle.listener.OnServiceCreateListener;
import eu.masconsult.malibu.lifecycle.listener.OnServiceStartListener;
import eu.masconsult.malibu.lifecycle.listener.OnStartListener;
import eu.masconsult.malibu.lifecycle.listener.OnStopListener;

public class LifecycleObjectHandler implements LifecycleContext {

    private final LinkedList<LifecycleInterface> objects = new LinkedList<LifecycleInterface>();
    private final Iterable<LifecycleInterface> listeners = Iterables.concat(
            GlobalLifecycleListeners.getListener(), objects);
    private final Iterable<LifecycleInterface> rlisteners = Iterables.concat(
            Lists.reverse(objects),
            GlobalLifecycleListeners.getReverseListener());
    private final HashMap<Class<?>, Iterable<Class<?>>> listenersCache = new HashMap<Class<?>, Iterable<Class<?>>>();
    private final HashMap<Class<?>, Iterable<Class<?>>> rlistenersCache = new HashMap<Class<?>, Iterable<Class<?>>>(
            2);

    @Override
    public void registerLifecycleListener(final LifecycleInterface lifecycleObject) {
        objects.add(lifecycleObject);
    }

    @SuppressWarnings("unchecked")
    private <T> Iterable<T> listeners(
            Class<T> listenerInterface,
            Iterable<LifecycleInterface> listeners,
            HashMap<Class<?>, Iterable<Class<?>>> cache) {
        Iterable<T> result = (Iterable<T>) cache.get(listenerInterface);
        if (result == null) {
            result = Iterables.filter(listeners, listenerInterface);
            cache.put(listenerInterface, (Iterable<Class<?>>) result);
        }
        return result;
    }

    private <T> Iterable<T> listeners(Class<T> listenerInterface) {
        return listeners(listenerInterface, listeners, listenersCache);
    }

    private <T> Iterable<T> rlisteners(Class<T> listenerInterface) {
        return listeners(listenerInterface, rlisteners, rlistenersCache);
    }

    public void onCreate(Bundle savedInstanceState) {
        for (OnCreateListener listener : listeners(OnCreateListener.class)) {
            listener.onCreate(savedInstanceState);
        }
    }

    public void onResume() {
        for (OnResumeListener listener : listeners(OnResumeListener.class)) {
            listener.onResume();
        }
    }

    public void onPause() {
        for (OnPauseListener listener : rlisteners(OnPauseListener.class)) {
            listener.onPause();
        }
    }

    public void onDestroy() {
        for (OnDestroyListener listener : rlisteners(OnDestroyListener.class)) {
            listener.onDestroy();
        }

        objects.clear();
    }

    public void onSaveInstanceState(Bundle outState) {
        for (OnSaveInstanceStateListener listener : listeners(OnSaveInstanceStateListener.class)) {
            listener.onSaveInstanceState(outState);
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        for (OnRestoreInstanceStateListener listener : listeners(OnRestoreInstanceStateListener.class)) {
            listener.onRestoreInstanceState(savedInstanceState);
        }
    }

    public void onBeforeSetContentView(Activity activity) {
        for (OnBeforeSetContentViewListener listener : listeners(OnBeforeSetContentViewListener.class)) {
            listener.onBeforeSetContentView(activity);
        }
    }

    public void onAfterSetContentView(Activity activity) {
        for (OnAfterSetContentViewListener listener : listeners(OnAfterSetContentViewListener.class)) {
            listener.onAfterSetContentView(activity);
        }
    }

    public void onStart() {
        for (OnStartListener listener : listeners(OnStartListener.class)) {
            listener.onStart();
        }
    }

    public void onStop() {
        for (OnStopListener listener : listeners(OnStopListener.class)) {
            listener.onStop();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        for (OnCreateOptionsMenuListener listener : listeners(OnCreateOptionsMenuListener.class)) {
            listener.onCreateOptionsMenu(menu, menuInflater);
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        for (OnPrepareOptionsMenuListener listener : listeners(OnPrepareOptionsMenuListener.class)) {
            listener.onPrepareOptionsMenu(menu);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = false;
        for (OnOptionsItemSelectedListener listener : listeners(OnOptionsItemSelectedListener.class)) {
            result |= listener.onOptionsItemSelected(item);
        }
        return result;
    }

    public void onOptionsMenuClosed(Menu menu) {
        for (OnOptionsMenuClosedListener listener : listeners(OnOptionsMenuClosedListener.class)) {
            listener.onOptionsMenuClosed(menu);
        }
    }

    public boolean hasOptionsMenu() {
        boolean result = false;
        for (final Object o : objects) {
            if (o instanceof HasOptionsMenuListener) {
                result |= ((HasOptionsMenuListener) o).hasOptionsMenu();
            } else if (o instanceof OnCreateOptionsMenuListener) {
                result |= true;
            }
        }
        return result;
    }

    public void onServiceCreate() {
        for (OnServiceCreateListener listener : listeners(OnServiceCreateListener.class)) {
            listener.onServiceCreate();
        }
    }

    public void onServiceStart(Intent intent, int flags, int startId) {
        for (OnServiceStartListener listener : listeners(OnServiceStartListener.class)) {
            listener.onServiceStart(intent, flags, startId);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (OnActivityResultListener listener : listeners(OnActivityResultListener.class)) {
            listener.onActivityResult(requestCode, resultCode, data);
        }
    }

}
