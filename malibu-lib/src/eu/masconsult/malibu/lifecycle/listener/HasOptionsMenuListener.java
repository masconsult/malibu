
package eu.masconsult.malibu.lifecycle.listener;

public interface HasOptionsMenuListener extends LifecycleInterface {

    public boolean hasOptionsMenu();
}
