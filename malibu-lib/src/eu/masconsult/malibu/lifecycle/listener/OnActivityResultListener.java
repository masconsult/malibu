
package eu.masconsult.malibu.lifecycle.listener;

import android.content.Intent;

public interface OnActivityResultListener extends LifecycleInterface {

    public void onActivityResult(int requestCode, int resultCode, Intent data);
}
