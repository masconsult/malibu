
package eu.masconsult.malibu.lifecycle.listener;

import android.app.Activity;

public interface OnAfterSetContentViewListener extends LifecycleInterface {

    public void onAfterSetContentView(Activity activity);
}
