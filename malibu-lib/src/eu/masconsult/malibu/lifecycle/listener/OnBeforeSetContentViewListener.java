
package eu.masconsult.malibu.lifecycle.listener;

import android.app.Activity;

public interface OnBeforeSetContentViewListener extends LifecycleInterface {

    public void onBeforeSetContentView(Activity activity);

}
