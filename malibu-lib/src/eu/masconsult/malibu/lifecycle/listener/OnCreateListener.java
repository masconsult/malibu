
package eu.masconsult.malibu.lifecycle.listener;

import android.os.Bundle;

public interface OnCreateListener extends LifecycleInterface {

    void onCreate(Bundle savedInstanceState);

}
