
package eu.masconsult.malibu.lifecycle.listener;

import android.view.Menu;
import android.view.MenuInflater;

public interface OnCreateOptionsMenuListener extends LifecycleInterface {

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater);
}
