
package eu.masconsult.malibu.lifecycle.listener;

public interface OnDestroyListener extends LifecycleInterface {

    public void onDestroy();
}
