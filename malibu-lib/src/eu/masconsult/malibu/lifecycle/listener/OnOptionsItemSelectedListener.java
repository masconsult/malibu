
package eu.masconsult.malibu.lifecycle.listener;

import android.view.MenuItem;

public interface OnOptionsItemSelectedListener extends LifecycleInterface {

    public boolean onOptionsItemSelected(MenuItem item);
}
