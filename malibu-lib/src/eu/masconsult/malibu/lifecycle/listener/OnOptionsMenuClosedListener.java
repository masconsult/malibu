
package eu.masconsult.malibu.lifecycle.listener;

import android.view.Menu;

public interface OnOptionsMenuClosedListener extends LifecycleInterface {

    public void onOptionsMenuClosed(Menu menu);
}
