
package eu.masconsult.malibu.lifecycle.listener;

public interface OnPauseListener extends LifecycleInterface {

    public void onPause();
}
