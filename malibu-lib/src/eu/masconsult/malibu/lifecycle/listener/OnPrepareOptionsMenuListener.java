
package eu.masconsult.malibu.lifecycle.listener;

import android.view.Menu;

public interface OnPrepareOptionsMenuListener extends LifecycleInterface {

    public void onPrepareOptionsMenu(Menu menu);
}
