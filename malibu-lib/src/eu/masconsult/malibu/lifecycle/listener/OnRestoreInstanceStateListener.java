
package eu.masconsult.malibu.lifecycle.listener;

import android.os.Bundle;

public interface OnRestoreInstanceStateListener extends LifecycleInterface {

    public void onRestoreInstanceState(Bundle savedInstanceState);
}
