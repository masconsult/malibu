
package eu.masconsult.malibu.lifecycle.listener;

public interface OnResumeListener extends LifecycleInterface {

    public void onResume();
}
