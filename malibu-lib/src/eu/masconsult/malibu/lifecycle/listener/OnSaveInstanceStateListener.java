
package eu.masconsult.malibu.lifecycle.listener;

import android.os.Bundle;

public interface OnSaveInstanceStateListener extends LifecycleInterface {

    public void onSaveInstanceState(Bundle outState);
}
