
package eu.masconsult.malibu.lifecycle.listener;

public interface OnServiceCreateListener extends LifecycleInterface {

    public void onServiceCreate();
}
