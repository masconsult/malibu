
package eu.masconsult.malibu.lifecycle.listener;

import android.content.Intent;

public interface OnServiceStartListener extends LifecycleInterface {

    public void onServiceStart(Intent intent, int flags, int startId);
}
