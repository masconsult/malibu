
package eu.masconsult.malibu.lifecycle.listener;

public interface OnStartListener extends LifecycleInterface {

    public void onStart();
}
