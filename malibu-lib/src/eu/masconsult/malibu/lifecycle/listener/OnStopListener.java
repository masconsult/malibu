
package eu.masconsult.malibu.lifecycle.listener;

public interface OnStopListener extends LifecycleInterface {

    public void onStop();
}
