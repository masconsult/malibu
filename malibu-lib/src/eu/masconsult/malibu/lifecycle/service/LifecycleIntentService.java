
package eu.masconsult.malibu.lifecycle.service;

import android.app.IntentService;
import android.content.Intent;
import eu.masconsult.malibu.lifecycle.internal.LifecycleObjectHandler;
import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;

public abstract class LifecycleIntentService extends IntentService implements
        LifecycleServiceContext {

    private LifecycleObjectHandler stateHolder = new LifecycleObjectHandler();

    public LifecycleIntentService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        stateHolder.onServiceCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int result = super.onStartCommand(intent, flags, startId);
        stateHolder.onServiceStart(intent, flags, startId);
        return result;
    }

    @Override
    public void onDestroy() {
        stateHolder.onDestroy();
        super.onDestroy();
    }

    @Override
    public void registerLifecycleListener(LifecycleInterface object) {
        stateHolder.registerLifecycleListener(object);
    }
}
