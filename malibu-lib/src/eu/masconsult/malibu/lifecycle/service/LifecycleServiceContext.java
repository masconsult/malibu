
package eu.masconsult.malibu.lifecycle.service;

import eu.masconsult.malibu.lifecycle.listener.LifecycleInterface;

public interface LifecycleServiceContext {

    /**
     * The supplied object should implement some of
     * {@link eu.masconsult.malibu.lifecycle.listener} listeners
     * 
     * @param object
     */
    void registerLifecycleListener(LifecycleInterface object);

}
