
package eu.masconsult.malibu.markets;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import eu.masconsult.malibu.R;

public enum Markets {

    //
    GOOGLE_PLAY("market://details?id=%s", "https://play.google.com/store/apps/details?id=%s"),
    //
    AMAZON("amzn://apps/android?p=%s", "http://www.amazon.com/gp/mas/dl/android?p=%s"),
    //
    SAMSUNG("samsungapps://ProductDetail/", null),
    //
    NOOK(null, null),
    //
    SLIDEME(null, null);

    private final String marketUriPrefix;
    private final String siteUriPrefix;

    private Markets(String uriPrefix, String sitePrefix) {
        marketUriPrefix = uriPrefix;
        siteUriPrefix = sitePrefix;
    }

    public Uri getMarketUri(Context context) {
        if (marketUriPrefix != null) {
            return Uri.parse(String.format(marketUriPrefix, context.getPackageName()));
        }
        return null;
    }

    public Intent getMarketIntent(Context context) {
        return new Intent(Intent.ACTION_VIEW).setData(getMarketUri(context));
    }

    public Uri getWebUri(Context context) {
        if (siteUriPrefix != null) {
            return Uri.parse(String.format(siteUriPrefix, context.getPackageName()));
        }
        return null;
    }

    public Intent getWebIntent(Context context) {
        return new Intent(Intent.ACTION_VIEW).setData(getWebUri(context));
    }

    public boolean isInstalled(Context context) {
        final Intent intent = getMarketIntent(context);
        final List<ResolveInfo> infos = context.getPackageManager()
                .queryIntentActivities(intent, 0);
        return infos.size() > 0;
    }

    public void open(Context context) {
        context.startActivity(getMarketIntent(context).addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_INCLUDE_STOPPED_PACKAGES));
    }

    public void openWeb(Context context) {
        if (siteUriPrefix != null && !siteUriPrefix.equals("")) {
            context.startActivity(getWebIntent(context).addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_INCLUDE_STOPPED_PACKAGES));
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.market_not_found);
            builder.show();
        }
    }
}
