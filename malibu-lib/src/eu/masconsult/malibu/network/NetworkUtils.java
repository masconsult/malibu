
package eu.masconsult.malibu.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.inject.Inject;

/**
 * A collection of network related utility methods.
 * 
 * @author Geno Roupsky &lt;geno&#064;masconsult.eu&gt;
 */
public class NetworkUtils {

    private Context context;

    @Inject
    public NetworkUtils(Context context) {
        this.context = context;
    }

    public ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     * @return true if any kind of connection to internet exists, false
     *         otherwise
     */
    public boolean isConnected() {
        final ConnectivityManager connMgr = getConnectivityManager();
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public boolean isWifiConnected() {
        final ConnectivityManager connMgr = getConnectivityManager();
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo != null && networkInfo.isConnected();
    }

    public boolean isMobileConnected() {
        final ConnectivityManager connMgr = getConnectivityManager();
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return networkInfo != null && networkInfo.isConnected();
    }
}
