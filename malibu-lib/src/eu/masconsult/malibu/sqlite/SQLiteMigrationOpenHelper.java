package eu.masconsult.malibu.sqlite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * A helper class to manage database creation, version management and automatic database
 * creation/upgrade using migrations from assets.
 * 
 * <p>
 * You usually instantiate, and this class takes care of opening the database if it exists, creating
 * it if it does not, and upgrading it as necessary. Transactions are used to make sure the database
 * is always in a sensible state.
 * 
 * <p>
 * This class makes it easy for {@link android.content.ContentProvider} implementations to defer
 * opening and upgrading the database until first use, to avoid blocking application startup with
 * long-running database upgrades.
 * 
 * <p>
 * This class examines the assets' migration directory ( {@link #DEFAULT_MIGRATION_FOLDER} by
 * default) and determines the actual database version that is required. If the database doesn't
 * exists or is lower version than the required, the database is create and upgraded as necessary.
 * 
 * <p>
 * The migrations should follow the style <strong>migration_###.sql</strong> where the numbers must
 * be consecutive and must correspond to the database version they upgrade to. For example
 * <strong>migration_1.sql</strong> is used to create the database if it doesn't exists,
 * <strong>migration_2.sql</strong> updates the database from version 1 to version 2 and so on. The
 * migration number can be zero-padded as they are checked as integers rather than strings. For
 * example <strong>migration_001.sql</strong> and <strong>migration_1.sql</strong> both mean
 * migration 1.
 * 
 * <p class="note">
 * <strong>Note:</strong> this class assumes monotonically increasing version numbers for upgrades.
 * </p>
 * <p class="note">
 * <strong>Note:</strong> when creating database all migrations are executed.
 * </p>
 */
public class SQLiteMigrationOpenHelper extends SQLiteOpenHelper {

	private static final String TAG = SQLiteMigrationOpenHelper.class.getSimpleName();
	public static final String DEFAULT_MIGRATION_FOLDER = "migrations";
	private static final String MIGRATION_FILE_PREFIX = "migration_";
	private static final String MIGRATION_FILE_PATTERN = "migration_([0-9]+).sql";

	private static HashMap<String, Integer> databaseVersions = new HashMap<String, Integer>(2);

	private Context context;
	private String migrationsFolder;

	public SQLiteMigrationOpenHelper(Context context, String name) {
		this(context, name, DEFAULT_MIGRATION_FOLDER);
	}

	public SQLiteMigrationOpenHelper(Context context, String name, String migrationsFolder) {
		super(context, name, null, getDatabaseVersion(migrationsFolder, context));
		this.context = context;
		this.migrationsFolder = migrationsFolder;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		onUpgrade(db, 0, getDatabaseVersion(migrationsFolder, context));
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			String[] migrationsList = context.getAssets().list(migrationsFolder);
			Arrays.sort(migrationsList);

			Pattern fileMatchPattern = Pattern.compile(MIGRATION_FILE_PATTERN);

			for (String migration : migrationsList) {
				final Matcher matcher = fileMatchPattern.matcher(migration);
				if (matcher.find()) {
					String migrationVersionStr = matcher.group(1);
					if (Integer.valueOf(migrationVersionStr) > oldVersion
							&& Integer.valueOf(migrationVersionStr) <= newVersion) {
						executeMigration(db, migration);
					}
				}
			}

		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

	}

	/**
	 * Executes sql statements in single migration file
	 * 
	 * @param db
	 * @param migrationFileName
	 * @throws IOException
	 */
	protected void executeMigration(SQLiteDatabase db, String migrationFileName) throws IOException {
		Log.d(TAG, "[" + migrationsFolder + "] Applying migration: " + migrationFileName);
		InputStream is = context.getAssets().open(migrationsFolder + "/" + migrationFileName);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sql = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			sql.append(line);
		}

		String[] statements = sql.toString().split(";");

		for (String statement : statements) {
			if (!statement.trim().equals("")) {
				Log.d(TAG, "[" + migrationsFolder + "] perform sql statement: " + statement);
				db.execSQL(statement.toString() + ";");
			}
		}

	}

	/**
	 * Calculate highest db version depending on migration files
	 * 
	 * @param migrationsFolder
	 * @param context
	 * @return
	 */
	private static int getDatabaseVersion(String migrationsFolder, Context context) {
		if (databaseVersions.containsKey(migrationsFolder)) {
			return databaseVersions.get(migrationsFolder);
		}
		Log.d(TAG, "retrieving db version.");
		int version = 0;
		try {
			String[] migrations = context.getAssets().list(migrationsFolder);
			for (String migration : migrations) {
				if (migration.startsWith(MIGRATION_FILE_PREFIX)) {
					version++;
				}
			}
			Log.d(TAG, "DB version: " + version);

		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		databaseVersions.put(migrationsFolder, version);
		return version;
	}

}
