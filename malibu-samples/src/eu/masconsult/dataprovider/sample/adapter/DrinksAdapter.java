package eu.masconsult.dataprovider.sample.adapter;

import java.util.List;


import eu.masconsult.dataprovider.sample.model.Drink;
import android.R;
import android.content.Context;
import android.widget.ArrayAdapter;

public class DrinksAdapter extends ArrayAdapter<Drink> {

	public DrinksAdapter(Context context) {
		super(context, R.layout.simple_list_item_1,  R.id.text1);
	}
	
	public void setData(List<Drink> data) {
        clear();
        if (data != null) {
            for (Drink drink : data) {
				add(drink);
			}
        }
    }

}
