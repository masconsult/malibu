package eu.masconsult.dataprovider.sample.adapter;

import java.util.List;


import eu.masconsult.dataprovider.sample.model.Element;
import android.R;
import android.content.Context;
import android.widget.ArrayAdapter;

public class ElementsAdapter extends ArrayAdapter<Element> {

	public ElementsAdapter(Context context) {
		super(context, R.layout.simple_list_item_1,  R.id.text1);
	}
	
	public void setData(List<Element> data) {
        clear();
        if (data != null) {
            for (Element element : data) {
				add(element);
			}
        }
    }

}
