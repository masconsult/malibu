package eu.masconsult.dataprovider.sample.adapter;

import java.util.List;

import android.R;
import android.content.Context;
import android.widget.ArrayAdapter;
import eu.masconsult.dataprovider.sample.model.Sport;

public class SportsAdapter extends ArrayAdapter<Sport> {

	public SportsAdapter(Context context) {
		super(context, R.layout.simple_list_item_1, R.id.text1);
	}

	public void setData(List<Sport> data) {
		clear();
		if (data != null) {
			for (Sport sport : data) {
				add(sport);
			}
		}
	}

}
