package eu.masconsult.dataprovider.sample.model;

public enum ElementType {
	TEXT, PASSWORD, CHECKBOX;
}
