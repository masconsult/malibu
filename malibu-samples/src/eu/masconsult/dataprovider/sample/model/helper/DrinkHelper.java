package eu.masconsult.dataprovider.sample.model.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import eu.masconsult.dataprovider.sample.model.Drink;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class DrinkHelper extends ModelHelper<Drink> {

	public static final String TABLE_NAME = "drinks";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";

	private static final String URL = "http://s3-eu-west-1.amazonaws.com/masconsult-tasks/task1-data.json";

	public static final String DATABASE_CREATE = "create table if not exists " + TABLE_NAME + "("
	//
			+ COLUMN_ID + " integer , "
			//
			+ COLUMN_NAME + " text);";

	@Override
	public void cache(List<Drink> drinks) {
		for (Drink drink : drinks) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ID, drink.getId());
			values.put(COLUMN_NAME, drink.getName());
			db.insert(TABLE_NAME, null, values);
		}
	}

	@Override
	public List<Drink> fetch() {
		Cursor cursor = db.query(TABLE_NAME, new String[] { COLUMN_ID, COLUMN_NAME }, null, null,
				null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() <= 0) {
			cursor.close();
			return null;
		} else {
			List<Drink> result = new ArrayList<Drink>(cursor.getCount());
			while (!cursor.isAfterLast()) {
				final Drink drink = new Drink();
				drink.setId(cursor.getInt(0));
				drink.setName(cursor.getString(1));
				result.add(drink);
				cursor.moveToNext();
			}
			cursor.close();
			return result;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getCreateTableString() {
		return DATABASE_CREATE;
	}

	@Override
	public List<Drink> retrieveDataFromServer(HttpClient client) throws ClientProtocolException,
			IOException, JSONException {
		HttpGet request = new HttpGet(URL);
		request.addHeader("Content-Type", "application/json");
		HttpResponse response = client.execute(request);
		String stringResponse = EntityUtils.toString(response.getEntity());
		return mapDrinksFromResponse(stringResponse);
	}

	private List<Drink> mapDrinksFromResponse(String json) throws JSONException {
		List<Drink> drinks = new ArrayList<Drink>();

		JSONArray drinksJson = new JSONArray(json);
		for (int i = 0; i < drinksJson.length(); i++) {
			JSONObject drinkJson = drinksJson.getJSONObject(i);
			Drink drink = new Drink();
			drink.setId(drinkJson.getInt("id"));
			drink.setName(drinkJson.getString("name"));
			drinks.add(drink);
		}

		return drinks;
	}

}
