package eu.masconsult.dataprovider.sample.model.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import eu.masconsult.dataprovider.sample.model.Element;
import eu.masconsult.dataprovider.sample.model.ElementType;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class ElementHelper extends ModelHelper<Element> {

	public static final String TABLE_NAME = "elements";
	public static final String COLUMN_ELEMENT_TYPE = "type";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_CHECKED = "checked";

	private static final String URL = "http://s3-eu-west-1.amazonaws.com/masconsult-tasks/task2-data.json";

	public static final String DATABASE_CREATE = "create table if not exists " + TABLE_NAME + "("
	//
			+ COLUMN_ELEMENT_TYPE + " text, "
			//
			+ COLUMN_NAME + " text , "
			//
			+ COLUMN_CHECKED + " integer );";

	@Override
	public void cache(List<Element> elements) {
		for (Element element : elements) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ELEMENT_TYPE, element.getType().toString());
			values.put(COLUMN_NAME, element.getName());
			values.put(COLUMN_CHECKED, element.isChecked() ? 1 : 0);
			db.insert(TABLE_NAME, null, values);
		}
	}

	@Override
	public List<Element> fetch() {
		Cursor cursor = db.query(TABLE_NAME, new String[] { COLUMN_ELEMENT_TYPE, COLUMN_NAME,
				COLUMN_CHECKED }, null, null, null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() <= 0) {
			cursor.close();
			return null;
		} else {
			List<Element> result = new ArrayList<Element>(cursor.getCount());
			while (!cursor.isAfterLast()) {
				final Element element = new Element();
				element.setType(ElementType.valueOf(cursor.getString(0)));
				element.setName(cursor.getString(1));
				element.setChecked((cursor.getInt(2) == 1) ? true : false);
				result.add(element);
				cursor.moveToNext();
			}
			cursor.close();
			return result;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getCreateTableString() {
		return DATABASE_CREATE;
	}

	@Override
	public List<Element> retrieveDataFromServer(HttpClient client) throws ClientProtocolException,
			IOException, JSONException {
		HttpGet request = new HttpGet(URL);
		request.addHeader("Content-Type", "application/json");
		HttpResponse response = client.execute(request);
		String stringResponse = EntityUtils.toString(response.getEntity());
		return mapElementsFromResponse(stringResponse);
	}

	private List<Element> mapElementsFromResponse(String jsonString) throws JSONException {
		List<Element> elements = new ArrayList<Element>();

		JSONObject json = new JSONObject(jsonString);
		JSONArray elementsJson = json.getJSONArray("inputs");

		for (int i = 0; i < elementsJson.length(); i++) {
			JSONObject elementJson = elementsJson.getJSONObject(i);
			Element element = new Element();
			element.setType(ElementType.valueOf(elementJson.getString("type").toUpperCase()));
			element.setName(elementJson.getString("name"));
			element.setChecked(elementJson.has("checked") ? elementJson.getBoolean("checked") : false);
			elements.add(element);
		}

		return elements;
	}
}
