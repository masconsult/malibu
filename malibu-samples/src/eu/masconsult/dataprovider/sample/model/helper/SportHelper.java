package eu.masconsult.dataprovider.sample.model.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import eu.masconsult.dataprovider.sample.model.Sport;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class SportHelper extends ModelHelper<Sport> {

	public static final String TABLE_NAME = "sports";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";

	public static final String USERNAME = "despark";

	public static final String PASSWORD = "sp0ck.d3v";

	public static final String HOST = "staging.despark.com";

	private static final String URL = "http://staging.despark.com/otos/main/get_sports_for_app";

	public static final String DATABASE_CREATE = "create table if not exists " + TABLE_NAME + "("
	//
			+ COLUMN_ID + " integer , "
			//
			+ COLUMN_NAME + " text);";

	@Override
	public void cache(List<Sport> sports) {
		for (Sport sport : sports) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ID, sport.getId());
			values.put(COLUMN_NAME, sport.getName());
			db.insert(TABLE_NAME, null, values);
		}
	}

	@Override
	public List<Sport> fetch() {
		Cursor cursor = db.query(TABLE_NAME, new String[] { COLUMN_ID, COLUMN_NAME }, null, null,
				null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() <= 0) {
			cursor.close();
			return null;
		} else {
			List<Sport> result = new ArrayList<Sport>(cursor.getCount());
			while (!cursor.isAfterLast()) {
				final Sport sport = new Sport();
				sport.setId(cursor.getInt(0));
				sport.setName(cursor.getString(1));
				result.add(sport);
				cursor.moveToNext();
			}
			cursor.close();
			return result;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getCreateTableString() {
		return DATABASE_CREATE;
	}

	@Override
	public List<Sport> retrieveDataFromServer(HttpClient client) throws ClientProtocolException,
			IOException, JSONException {
		HttpGet request = new HttpGet(URL);
		request.addHeader("Content-Type", "application/json");
		((DefaultHttpClient) client).getCredentialsProvider().setCredentials(
				new AuthScope(HOST, -1), new UsernamePasswordCredentials(USERNAME, PASSWORD));
		HttpResponse response = client.execute(request);
		String stringResponse = EntityUtils.toString(response.getEntity());
		return mapResponse(stringResponse);
	}

	private List<Sport> mapResponse(String json) throws JSONException {
		List<Sport> sports = new ArrayList<Sport>();

		JSONArray sportsJson = new JSONArray(json);
		for (int i = 0; i < sportsJson.length(); i++) {
			JSONObject sportJson = sportsJson.getJSONObject(i);
			Sport sport = new Sport();
			sport.setId(sportJson.getInt("id"));
			sport.setName(sportJson.getString("name"));
			sports.add(sport);
		}

		return sports;
	}

}
