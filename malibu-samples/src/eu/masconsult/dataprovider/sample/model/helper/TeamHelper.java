package eu.masconsult.dataprovider.sample.model.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import eu.masconsult.dataprovider.sample.model.Team;
import eu.masconsult.malibu.dataprovider.model.helper.ModelHelper;

public class TeamHelper extends ModelHelper<Team> {

	public static final String TABLE_NAME = "teams";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_SPORT = "sport";

	public static final String USERNAME = "despark";

	public static final String PASSWORD = "sp0ck.d3v";

	public static final String HOST = "staging.despark.com";

	private static final String URL = "http://staging.despark.com/otos/main/get_teams_by_sport";

	public static final String DATABASE_CREATE = "create table if not exists " + TABLE_NAME + "("
	//
			+ COLUMN_ID + " integer , "
			//
			+ COLUMN_NAME + " text ,"
			//
			+ COLUMN_SPORT + " integer );";

	private int sportId = -1;

	@Override
	public String getClearCacheFilter() {
		return COLUMN_SPORT + "=?";
	}

	@Override
	public String[] getClearCacheParams() {
		return new String[] { String.valueOf(sportId) };
	}

	@Override
	public void cache(List<Team> teams) {
		for (Team team : teams) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ID, team.getId());
			values.put(COLUMN_NAME, team.getName());
			values.put(COLUMN_SPORT, sportId);
			db.insert(TABLE_NAME, null, values);
		}
	}

	@Override
	public List<Team> fetch() {
		Cursor cursor = db.rawQuery(
				"SELECT * from " + TABLE_NAME + " WHERE " + COLUMN_SPORT + "=?",
				new String[] { String.valueOf(sportId) });
		cursor.moveToFirst();
		if (cursor.getCount() <= 0) {
			cursor.close();
			return null;
		} else {
			List<Team> result = new ArrayList<Team>(cursor.getCount());
			while (!cursor.isAfterLast()) {
				final Team team = new Team();
				team.setId(cursor.getInt(0));
				team.setName(cursor.getString(1));
				result.add(team);
				cursor.moveToNext();
			}
			cursor.close();
			System.out.println(result);
			return result;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getCreateTableString() {
		return DATABASE_CREATE;
	}

	@Override
	public List<Team> retrieveDataFromServer(HttpClient client) throws ClientProtocolException,
			IOException, JSONException {
		if (sportId != -1) {
			HttpPost request = new HttpPost(URL);
			((DefaultHttpClient) client).getCredentialsProvider().setCredentials(
					new AuthScope(HOST, -1), new UsernamePasswordCredentials(USERNAME, PASSWORD));
			List<NameValuePair> formParameter = new ArrayList<NameValuePair>();
			formParameter.add(new BasicNameValuePair("sport", String.valueOf(sportId)));
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParameter, "UTF-8");
			request.setEntity(entity);
			HttpResponse response = client.execute(request);
			String stringResponse = EntityUtils.toString(response.getEntity());
			return mapResponse(stringResponse);
		} else {
			return null;
		}
	}

	private List<Team> mapResponse(String json) throws JSONException {
		List<Team> teams = new ArrayList<Team>();

		JSONArray teamsJson = new JSONArray(json);
		for (int i = 0; i < teamsJson.length(); i++) {
			JSONObject teamJson = teamsJson.getJSONObject(i);
			Team team = new Team();
			team.setId(teamJson.getInt("id"));
			team.setName(teamJson.getString("name"));
			teams.add(team);
		}

		return teams;
	}

	public void setSportId(int id) {
		sportId = id;
	}

}
