
package eu.masconsult.malibu.dataprovider.sample;

import android.app.Activity;
import android.content.Context;
import android.view.Window;
import eu.masconsult.malibu.lifecycle.LifecycleContext;
import eu.masconsult.malibu.lifecycle.listener.OnAfterSetContentViewListener;
import eu.masconsult.malibu.lifecycle.listener.OnBeforeSetContentViewListener;
import eu.masconsult.malibu.samples.R;

public class CustomTitle implements OnBeforeSetContentViewListener,
        OnAfterSetContentViewListener {

    public CustomTitle(Context context) {
        if (context instanceof LifecycleContext) {
            ((LifecycleContext) context).registerLifecycleListener(this);
        }
    }

    @Override
    public void onBeforeSetContentView(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
    }

    @Override
    public void onAfterSetContentView(Activity activity) {
        activity.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                R.layout.dataprovider_title);
    }
}
