
package eu.masconsult.malibu.dataprovider.sample;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import eu.masconsult.dataprovider.sample.adapter.DrinksAdapter;
import eu.masconsult.dataprovider.sample.model.Drink;
import eu.masconsult.dataprovider.sample.model.helper.DrinkHelper;
import eu.masconsult.malibu.dataprovider.provider.DataProvider;
import eu.masconsult.malibu.dataprovider.provider.DataProviderListener;
import eu.masconsult.malibu.lifecycle.activity.LifecycleListActivity;
import eu.masconsult.malibu.samples.R;

public class DrinksActivity extends LifecycleListActivity implements
        DataProviderListener<Drink> {

    final DrinkHelper helper = new DrinkHelper();
    final DataProvider<Drink, DrinkHelper> provider = new DataProvider<Drink, DrinkHelper>(
            this, this, helper);
    final CustomTitle customTitle = new CustomTitle(this);

    DrinksAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dataprovider_list);

        init();
    }

    private void init() {
        adapter = new DrinksAdapter(this);
        setListAdapter(adapter);
        ((TextView) findViewById(R.id.title)).setText(R.string.app_name);
    }

    public void refresh(View view) {
        provider.refresh();
    }

    @Override
    public void onProviderStart() {
        ((ImageView) findViewById(R.id.refresh)).setVisibility(View.GONE);
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onProviderStop() {
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.refresh)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onProviderError() {
        Toast.makeText(this, "Service unavailable", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDataChanged(long timestamp, List<Drink> data) {
        adapter.setData(data);
    }

}
