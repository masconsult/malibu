
package eu.masconsult.malibu.dataprovider.sample;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import eu.masconsult.dataprovider.sample.adapter.ElementsAdapter;
import eu.masconsult.dataprovider.sample.model.Element;
import eu.masconsult.dataprovider.sample.model.helper.ElementHelper;
import eu.masconsult.malibu.dataprovider.provider.DataProvider;
import eu.masconsult.malibu.dataprovider.provider.DataProviderListener;
import eu.masconsult.malibu.lifecycle.activity.LifecycleListActivity;
import eu.masconsult.malibu.samples.R;

public class ElementsActivity extends LifecycleListActivity implements
        DataProviderListener<Element> {

    final ElementHelper helper = new ElementHelper();
    final DataProvider<Element, ElementHelper> provider = new DataProvider<Element, ElementHelper>(
            this, this, helper);
    final CustomTitle customTitle = new CustomTitle(this);

    ElementsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dataprovider_list);

        init();
    }

    private void init() {
        ((TextView) findViewById(R.id.title)).setText(R.string.app_name);
        adapter = new ElementsAdapter(this);
        setListAdapter(adapter);
    }

    public void refresh(View view) {
        provider.refresh();
    }

    @Override
    public void onProviderStart() {
        ((ImageView) findViewById(R.id.refresh)).setVisibility(View.GONE);
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onProviderStop() {
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.refresh)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onProviderError() {
        Toast.makeText(this, "Service unavailable", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDataChanged(long timestamp, List<Element> data) {
        adapter.setData(data);
    }

}
