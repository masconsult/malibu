
package eu.masconsult.malibu.dataprovider.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import eu.masconsult.malibu.samples.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dataprovider_main);
    }

    public void showDrinks(View view) {
        Intent intent = new Intent(this, DrinksActivity.class);
        startActivity(intent);
    }

    public void showElements(View view) {
        Intent intent = new Intent(this, ElementsActivity.class);
        startActivity(intent);
    }

    public void showOtos(View view) {
        Intent intent = new Intent(this, OtosActivity.class);
        startActivity(intent);
    }

}
