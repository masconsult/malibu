
package eu.masconsult.malibu.dataprovider.sample;

import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import eu.masconsult.dataprovider.sample.adapter.SportsAdapter;
import eu.masconsult.dataprovider.sample.adapter.TeamsAdapter;
import eu.masconsult.dataprovider.sample.model.Sport;
import eu.masconsult.dataprovider.sample.model.Team;
import eu.masconsult.dataprovider.sample.model.helper.SportHelper;
import eu.masconsult.dataprovider.sample.model.helper.TeamHelper;
import eu.masconsult.malibu.dataprovider.provider.DataProvider;
import eu.masconsult.malibu.dataprovider.provider.DataProviderListener;
import eu.masconsult.malibu.lifecycle.activity.LifecycleActivity;
import eu.masconsult.malibu.samples.R;

public class OtosActivity extends LifecycleActivity {

    DataProvider<Sport, SportHelper> sportsProvider;
    SportsAdapter sportsAdapter;
    SportHelper sportHelper;

    DataProvider<Team, TeamHelper> teamsProvider;
    TeamsAdapter teamsAdapter;
    TeamHelper teamHelper;

    final CustomTitle customTitle = new CustomTitle(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dataprovider_otos);

        init();
    }

    private void init() {
        ((TextView) findViewById(R.id.title)).setText("Otos");
        sportsAdapter = new SportsAdapter(this);
        sportHelper = new SportHelper();
        sportsProvider = new DataProvider<Sport, SportHelper>(this,
                sportsDataListener, sportHelper);

        teamsAdapter = new TeamsAdapter(this);
        teamHelper = new TeamHelper();
        teamsProvider = new DataProvider<Team, TeamHelper>(this,
                teamsDataListener, teamHelper);
    }

    public void showSports(View view) {
        AlertDialog.Builder builder = createAlertBuilder();

        if (sportsAdapter.isEmpty()) {
            sportsProvider.refresh();
        }

        builder.setTitle("sports");
        builder.setSingleChoiceItems(sportsAdapter, -1,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        teamHelper.setSportId(sportsAdapter.getItem(item)
                                .getId());
                        teamsProvider.refresh();
                        ((Button) findViewById(R.id.sports_btn))
                                .setText(sportsAdapter.getItem(item).getName());
                        ((Button) findViewById(R.id.teams_btn))
                                .setText(R.string.teams);
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private Builder createAlertBuilder() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return new AlertDialog.Builder(this);
        }
        return new AlertDialog.Builder(this,
                AlertDialog.THEME_DEVICE_DEFAULT_DARK);
    }

    public void showTeams(View view) {
        AlertDialog.Builder builder = createAlertBuilder();

        if (teamsAdapter.isEmpty()) {
            teamsProvider.refresh();
        }

        builder.setTitle("teams");
        builder.setSingleChoiceItems(teamsAdapter, -1,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        ((Button) findViewById(R.id.teams_btn))
                                .setText(teamsAdapter.getItem(item).getName());
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    public void refresh(View view) {

    }

    private DataProviderListener<Sport> sportsDataListener = new DataProviderListener<Sport>() {

        @Override
        public void onProviderStart() {
            ((ImageView) findViewById(R.id.refresh)).setVisibility(View.GONE);
            ((ProgressBar) findViewById(R.id.loading))
                    .setVisibility(View.VISIBLE);
        }

        @Override
        public void onProviderStop() {
            ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.GONE);
            ((ImageView) findViewById(R.id.refresh))
                    .setVisibility(View.VISIBLE);
        }

        @Override
        public void onProviderError() {
            Toast.makeText(OtosActivity.this, "Service unavailable",
                    Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onDataChanged(long timestamp, List<Sport> data) {
            sportsAdapter.setData(data);
        }
    };

    private DataProviderListener<Team> teamsDataListener = new DataProviderListener<Team>() {

        @Override
        public void onProviderStart() {
            ((ImageView) findViewById(R.id.refresh)).setVisibility(View.GONE);
            ((ProgressBar) findViewById(R.id.loading))
                    .setVisibility(View.VISIBLE);
        }

        @Override
        public void onProviderStop() {
            ((ProgressBar) findViewById(R.id.loading)).setVisibility(View.GONE);
            ((ImageView) findViewById(R.id.refresh))
                    .setVisibility(View.VISIBLE);
        }

        @Override
        public void onProviderError() {
            Toast.makeText(OtosActivity.this, "Service unavailable",
                    Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onDataChanged(long timestamp, List<Team> data) {
            teamsAdapter.setData(data);
        }
    };

}
