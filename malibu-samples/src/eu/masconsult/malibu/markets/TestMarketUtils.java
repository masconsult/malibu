
package eu.masconsult.malibu.markets;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class TestMarketUtils extends ListActivity implements OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);
        adapter.add("Installed from: "
                + getPackageManager().getInstallerPackageName(getPackageName()));
        for (final Markets market : Markets.values()) {
            adapter.add(market.name() + ": " + market.isInstalled(this));
        }
        setListAdapter(adapter);

        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        @SuppressWarnings("unchecked")
        String item = ((ArrayAdapter<String>) getListAdapter()).getItem(arg2);
        for (final Markets market : Markets.values()) {
            if (item.startsWith(market.name())) {
                market.open(this);
                break;
            }
        }
    }
}
