
package eu.masconsult.malibu.samples;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import eu.masconsult.malibu.markets.TestMarketUtils;

public class MainActivity extends ListActivity implements OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<Class<?>> adapter = new ArrayAdapter<Class<?>>(this,
                android.R.layout.simple_list_item_1);
        adapter.add(eu.masconsult.malibu.dataprovider.sample.MainActivity.class);
        adapter.add(TestMarketUtils.class);
        setListAdapter(adapter);

        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        startActivity(new Intent(this, (Class<?>) getListAdapter().getItem(arg2)));
    }

}
